//
//  EGTableViewCell.m
//  Eagan
//
//  Created by Thomas Schmidt on 2/21/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import "EGTableViewCell.h"
#import "UIColor+Eagan.h"
#import "UIFont+Eagan.h"

@interface EGTableViewCell ()

@property (strong, nonatomic) IBOutlet UITextView *textView;

@end

NSString* placeholderText =  @"Start typing.";

@implementation EGTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    [_textView setDelegate:self];
    _textView.layer.cornerRadius = 5;
    _textView.layer.masksToBounds = YES;
    _textView.textContainerInset = UIEdgeInsetsMake(8, 4, 8, 4);
    _textView.scrollEnabled = NO;
    
    
    self.contentView.backgroundColor = [UIColor cellBackgroundColor];
    _textView.font = [UIFont cellFont];
}

+ (EGTableViewCell*)tableViewCellWithData:(EGNoteData *)data {
    EGTableViewCell *tableViewCell = [[[NSBundle mainBundle] loadNibNamed:@"EGTableViewCell" owner:nil options:nil] lastObject];
    [tableViewCell setupWithData:data];
    return tableViewCell;
}

- (void)setupWithData:(EGNoteData *)data {
    _uuid = data.uuid;
    _textView.text = data.text;
    
    if([data.text isEqualToString:@""]) {
        self.textView.text = placeholderText;
        self.textView.textColor = [UIColor lightGrayColor];
    }
    
    [self layoutSubviews];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [_textView resignFirstResponder];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [_textView resignFirstResponder];
}

- (float)textViewHeight {
    return _textView.frame.size.height;
}

- (NSString*)textViewText {
    if([_textView.text isEqualToString:placeholderText]) {
        return @"";
    } else {
        return _textView.text;
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    [self layoutSubviews];
    [self.delegate cellTextDidChange:self];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:placeholderText]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = placeholderText;
        textView.textColor = [UIColor lightGrayColor];
    }
    [textView resignFirstResponder];
}

- (CGFloat)textViewHeightForAttributedText:(NSAttributedString *)text andWidth:(CGFloat)width
{
    UITextView *textView = [[UITextView alloc] init];
    [textView setAttributedText:text];
    CGSize size = [textView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    NSLog(@"Size of %f\t%f", size.width, size.height);
    return size.height;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    CGRect curFrame = _textView.frame;
    CGFloat estimatedHeight = [self textViewHeightForAttributedText:_textView.attributedText andWidth:_textView.textContainer.size.width];
    if(curFrame.size.height != estimatedHeight) {
        [UIView animateWithDuration:0.1f
                              delay:0
                            options:(UIViewAnimationOptionAllowUserInteraction|
                                     UIViewAnimationOptionBeginFromCurrentState)
                         animations:^(void) {
                             [self resizeTextView];
                         }
                         completion:^(BOOL finished) {
                                _textView.scrollEnabled = YES;
                                [_textView scrollRangeToVisible:NSMakeRange(_textView.text.length - 1, 0)];
                                _textView.scrollEnabled = NO;
                         }];
    } else {
        [self resizeTextView];
        _textView.scrollEnabled = YES;
        [_textView scrollRangeToVisible:NSMakeRange(_textView.text.length - 1, 0)];
        _textView.scrollEnabled = NO;
    }
}

- (void)resizeTextView {
    CGRect curFrame = _textView.frame;
    NSLog(@"Frame width A %f", curFrame.size.width);
    curFrame.size.height = [self textViewHeightForAttributedText:_textView.attributedText andWidth:_textView.textContainer.size.width];
    curFrame.size.width = self.frame.size.width - 10;
    curFrame.origin.x = (self.frame.size.width - curFrame.size.width) / 2.0;
    curFrame.origin.y = ((self.frame.size.height / 2) - curFrame.size.height / 2);
    _textView.frame = curFrame;
    NSLog(@"Frame width B %f", curFrame.size.width);
}

@end
