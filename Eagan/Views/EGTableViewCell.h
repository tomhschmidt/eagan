//
//  EGTableViewCell.h
//  Eagan
//
//  Created by Thomas Schmidt on 2/21/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"
#import "EGNoteData.h"

@class EGTableViewCell;

@protocol EGTableViewCellDelegate <NSObject>

- (void)cellTextDidChange:(EGTableViewCell*)tableViewCell;

@end


@interface EGTableViewCell : UITableViewCell<UITextViewDelegate, HPGrowingTextViewDelegate>

+ (EGTableViewCell*)tableViewCellWithData:(EGNoteData*)data;

- (void)setupWithData:(EGNoteData*)data;
- (float)textViewHeight;
- (NSString*)textViewText;

@property NSUUID* uuid;
@property NSObject<EGTableViewCellDelegate>* delegate;

@end