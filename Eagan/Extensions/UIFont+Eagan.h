//
//  UIFont+Eagan.h
//  Eagan
//
//  Created by Thomas Schmidt on 3/4/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Eagan)

+ (UIFont*)cellFont;

@end
