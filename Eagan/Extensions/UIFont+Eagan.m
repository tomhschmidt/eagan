//
//  UIFont+Eagan.m
//  Eagan
//
//  Created by Thomas Schmidt on 3/4/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import "UIFont+Eagan.h"

@implementation UIFont (Eagan)

+ (UIFont*)cellFont {
    return [UIFont fontWithName:@"Helvetica Neue" size:20];
}

@end
