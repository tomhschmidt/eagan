//
//  UIColor+Eagan.m
//  Eagan
//
//  Created by Thomas Schmidt on 3/4/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import "UIColor+Eagan.h"

@implementation UIColor (Eagan)

+ (UIColor*)cellBackgroundColor {
    return [UIColor colorWithRed:30/255.0 green:30/255.0 blue:30/255.0 alpha:1];
}

@end
