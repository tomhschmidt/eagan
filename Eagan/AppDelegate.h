//
//  AppDelegate.h
//  Eagan
//
//  Created by Thomas Schmidt on 2/21/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

