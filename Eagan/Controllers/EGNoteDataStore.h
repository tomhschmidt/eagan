//
//  EGNoteDataStore.h
//  Eagan
//
//  Created by Thomas Schmidt on 3/8/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EGNoteData.h"

@interface EGNoteDataStore : NSObject

- (void)setNote:(EGNoteData*)note forUUID:(NSUUID*)uuid;
- (EGNoteData*)dataWithUUID:(NSUUID*)uuid;

@end
