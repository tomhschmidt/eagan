//
//  EGNoteDataStore.m
//  Eagan
//
//  Created by Thomas Schmidt on 3/8/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import "EGNoteDataStore.h"

@interface EGNoteDataStore ()

@property NSMutableDictionary* uuidToNoteData;

@end

@implementation EGNoteDataStore

- (id)init {
    self = [super init];
    if(self) {
        _uuidToNoteData = [NSMutableDictionary dictionary];
    }
    
    return self;
}

- (void)setNote:(EGNoteData*)note forUUID:(NSUUID*)uuid {
    [_uuidToNoteData setObject:note forKey:uuid];
}

- (EGNoteData*)dataWithUUID:(NSUUID*)uuid {
    return [_uuidToNoteData objectForKey:uuid];
}

@end
