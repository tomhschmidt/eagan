
//
//  EGMainTableTableViewController.m
//  Eagan
//
//  Created by Thomas Schmidt on 2/21/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import "EGMainTableTableViewController.h"
#import "EGTableViewCell.h"
#import "EGNoteDataStore.h"
#import "UIColor+Eagan.h"

@interface EGMainTableTableViewController ()

@property NSMutableArray* uuidAtRow;
@property NSMutableDictionary* cellHeightForUUID;
@property EGNoteDataStore* dataStore;

@end

@implementation EGMainTableTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    self.tableView.backgroundColor = [UIColor cellBackgroundColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    gestureRecognizer.cancelsTouchesInView = NO;
    [self.tableView addGestureRecognizer:gestureRecognizer];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"EGTableViewCell" bundle:nil] forCellReuseIdentifier:@"EGTableViewCell"];
    
    _cellHeightForUUID = [NSMutableDictionary dictionary];
    _uuidAtRow = [NSMutableArray array];
    _dataStore = [[EGNoteDataStore alloc] init];
    
    for(int i = 0; i < 10; i++) {
        EGNoteData* data = [[EGNoteData alloc] init];
        [_dataStore setNote:data forUUID:data.uuid];
        [_uuidAtRow addObject:data.uuid];
        [_cellHeightForUUID setObject:[NSNumber numberWithFloat:40] forKey:data.uuid];
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)hideKeyboard {
    [self.view endEditing:NO];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_uuidAtRow count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EGTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EGTableViewCell" forIndexPath:indexPath];
    NSUUID* uuid = [_uuidAtRow objectAtIndex:indexPath.row];
    EGNoteData* data = [_dataStore dataWithUUID:uuid];
    
    if(!cell) {
        cell = [EGTableViewCell tableViewCellWithData:data];
    } else {
        [cell setupWithData:data];
    }
    
    cell.delegate = self;
    
    return cell;
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUUID* uuidAtPath = [_uuidAtRow objectAtIndex:indexPath.row];
    float cellHeight = [[_cellHeightForUUID objectForKey:uuidAtPath] floatValue];
    return cellHeight + 70;
}

- (void)cellTextDidChange:(EGTableViewCell *)tableViewCell {
    [_dataStore dataWithUUID:tableViewCell.uuid].text = [tableViewCell textViewText];
    NSLog(@"TextViewHeight for UUID %@ is %f", tableViewCell.uuid, [tableViewCell textViewHeight]);
    [_cellHeightForUUID setObject:[NSNumber numberWithFloat:[tableViewCell textViewHeight]] forKey:tableViewCell.uuid];
    [[self tableView] beginUpdates];
    [[self tableView] endUpdates];
}

@end
