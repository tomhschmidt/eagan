//
//  EGNoteData.m
//  Eagan
//
//  Created by Thomas Schmidt on 3/2/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import "EGNoteData.h"

@implementation EGNoteData

- (id)init {
    self = [super init];
    if(self) {
        self.uuid = [NSUUID UUID];
        self.text = @"";
    }
    
    return self;
}

@end
