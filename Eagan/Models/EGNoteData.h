//
//  EGNoteData.h
//  Eagan
//
//  Created by Thomas Schmidt on 3/2/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EGNoteData : NSObject

@property NSString* text;
@property NSUUID* uuid;
@property NSDate* creationDate;
@property NSDate* lastEditedDate;

@end
